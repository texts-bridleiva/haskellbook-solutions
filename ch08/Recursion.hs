module Recursion where

data DividedResult = Result Integer | DividedByZero
    -- deriving (Show)

instance Show DividedResult where
    show (Result i) = show i
    show DividedByZero = "ERROR: Division by zero is not defined"

-- Changed Eq a to Ord a so it can manage negative numbers
recSum :: (Ord a, Num a) => a -> a
recSum n | n <= 0 = 0
recSum n = go n 0
    where go num acc
            | num == 0 = acc
            | otherwise = go (num -1) (acc + num)

intgrlProd :: (Integral a) => a -> a -> a
intgrlProd i1 i2 
    | anyZero   = 0
    | i2 < 0    = go (-i1) (-i2) 0
    | otherwise = go i1 i2 0
    where 
        anyZero = i1 == 0 || i2 == 0
        go n1 n2 acc
            | n2 == 0   = acc
            | otherwise = go n1 (n2 - 1) (acc + n1)


dividedBy :: Integral a => a -> a -> (DividedResult, a)
dividedBy num denom
    | denom == 0 = (DividedByZero, 0)
    | num == 0   = if denom < 0 then (Result 0, -denom) else (Result 0, denom)
    | num < 0 && denom < 0  = dividedBy (-num) (-denom)
    | num < 0 && denom > 0  = negateFirst $ dividedBy (-num) denom
    | num > 0 && denom < 0  = negateFirst $ dividedBy num (-denom)
    | otherwise = go num denom 0
        where 
            go n d count
                | n < d = (Result count, n)
                | otherwise = go (n - d) d (count + 1)

            negateFirst (Result a, b) = (Result (-a),b)


mc91 :: Integer -> Integer
mc91 n | n > 100    = n - 10
    | otherwise     = mc91 $ mc91 $ n + 11