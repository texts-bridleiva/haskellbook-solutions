module WordNumber where

import Data.List (intersperse)


digitToWord :: Int -> String
digitToWord n = head . drop n $ names
    where names = ["zero","one","two","three","four","five","six","seven","eight","nine"]

digits :: Int -> [Int]
digits = map asInt . show
    where asInt x = read [x] :: Int

wordNumber :: Int -> String
wordNumber = concat . intersperse "-" . map digitToWord . digits
    