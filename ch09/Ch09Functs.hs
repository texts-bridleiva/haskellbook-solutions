module Ch09Functs where

import Data.Char (isUpper, toUpper)

upperOnly :: String -> String
upperOnly cs = [x | x <- cs, isUpper x]

upperFirst :: String -> String
upperFirst [] = []
upperFirst (c : cs) = toUpper c : cs

upperAll :: String -> String
upperAll [] = []
upperAll (c : cs) = toUpper c : upperAll cs

head' :: [Char] -> Char
head' (c : cs) = toUpper c

head'' :: [Char] -> Char
head'' = head . upperAll