module Cipher where

import Data.Char
import Data.List

ls = ['A'..'Z'] ++ ['z', 'y'..'a']

getIndex :: Eq a => a -> [a] -> Int
getIndex e l = case elemIndex e l of
    Nothing  -> 0
    Just num -> num

caesar :: String -> Int -> String
caesar [] _ = ""
caesar cs 0 = cs
caesar (c : cs) k = (encode c k) : (caesar cs k)
    where
        encode c k = head . drop (offsetBy c k) $ ls
        offsetBy c k = mod (k + indexOf c) (length ls)
        indexOf = flip getIndex ls

uncaesar :: String -> Int -> String
uncaesar cs l = caesar cs $ (length ls) - l