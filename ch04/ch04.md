# Solutions to chapter 4 exercises.

required definitions:

awesome = ["Papuchon", "curry", ":)"]
also = ["Quake", "The Simons"]
allAwesome = [awesome, also]

1. --length :: [a] -> Int-- length :: Foldable t => t a -> Int

2. 
    a) 5
    b) 3
    c) 2
    d) 5

3.  The line that will return an error is ```6 / length [1, 2, 3]```. The reason being that
the (/) operator works on the Fractional typeclass, as we can verify looking at its signature:
```(/) :: Fractional a => a -> a -> a``` while the length function returns an Int, which is not 
a member of the Fractional typeclass.

4. We can fix this by using the integer division function div: ```div 6 length [1, 2, 3]```.

5. Type boolean, we would expect it to be True.

6. The first statement is an assignment, and as such returns no value. The second is an expression
of type boolean that would return False.

7. 
    - length allAwesome == 2 works and evaluates True.
    - length [1, 'a', 3, 'b'] will not work because all elements of a list must be of the same type.
    - length allAwesome + length awesome works and reduces to the sum of the lengths of allAwesome and 
    awesome, this is, reduces to 5.
    - (8 == 8) && ('b' < 'a') works and reduces to False
    - (8 == 8) && 9 doesn't work because && takes two boolean arguments and here a number it's being passed.

8. 
```
isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome str = str == reverse str
```

9. 
```
myAbs :: Integer -> Integer
myAbs n = if n < 0 then (-n) else n
```

10. 
```
f :: (a, b) -> (c, d) -> ((b, d), (a, c))
f t1 t2 = ( (snd t1, snd t2), (fst t1, fst t2) )
```

## Correcting syntax

1. function names must start with lowercase, and backtics (instead of apostrophes) make an operator infix:
```
x = (+)
f xs = w `x` 1
    where w = length xs
```

2. The parameter is different to the variable, they must both be lowercase x: ```\x = x```.

3. Similar to the example above, casing is important: ```f (a, b) = a```.

## Match the function name with their types

1. c is the type of show.

2. b is the type of (==).

3. a is the type of fst.

4. d is the type of (+).