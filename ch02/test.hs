
-- ### In-chapter functions
sayHello :: String -> IO ()
sayHello x = putStrLn ( "Hello " ++ x ++ "!" )

-- triple :: Number -> Number
triple x = 3 * x


-- ### In-Chapter exercises
-- #### Comprehension check
-- 1. "let half x = x/2" and "let square x = x * x" should run in the repl.
half x = x/2
square x = x * x

-- 2. "3.14" is a constant and the rest of the body can be expressed in terms of square:
-- piTimesSquareOf x = 3.14 * (square x)

-- 3. ...
piTimesSquareOf x = pi * (square x)

-- #### Parentheses and association
-- 1. Yes, * has higher precedence.
-- 2. No, * has higher precedence, so the expression in parentheses would be evaluated first anyway.
-- 3. Yes, / has higher precedence.

-- #### Heal the sick
-- 1. The code does compile in the repl.
-- 2. The variable b is not bound and is never defined in the body of the function
--  let double x = x * 2 will fix the problem
-- 3. The second line has bad indentation, the fix should be:
--     x = 7
--     y = 10
--     f = x + y

-- TODO: Add testing.