# Solutions to chapter 5 exercises.

## Multiple choice

1. c

2. a

3. b

4. c


## Determine the type

1.  a) Num a => a
    b) Num a => (a, [char])
    c) (Integer, [char])
    d) Bool
    e) Int
    f) Bool

2. Num a => a

3. Num a => a -> a

4. [char]


## Does it compile?

1. Bignum is not a function, so it can't be applied to 10 and assigned to wahoo.

2. All expressions are valid.

3. b is not a function, but a number, so it cannot be applied to 10 and assigned to c.

4. c is a free variable, so it can't be multiplied by 10000 and assigned to b.


## Type variable or specific type constructor?

1. ...

2.  zed - Fully polymorphyc
    Zed - Concrete type constructor
    Blah - Concrete type constructor

3.  a - Fully polymorphyc
    b - Enum Constrained polymorphyc
    C - Concrete type constructor

4.  f - Fully polymorphyc
    g - Fully polymorphyc
    C - Concrete type constructor
    

## Write a type signature

1. functionH :: [a] -> a

2. functionC :: Ord a => a -> a -> Bool

3. functionS :: (a, b) -> b


## Given a type, write the function

1. i = id

2. c a _ = a

3. yes, both return the first argument and ignore the second.

4. ```c' = flip c``` or simply ```c' _ b = b``` but the first form would make sense of the naming.

5. One posibility is returning the same list. The other, slightly more interesting is to reverse it:
```r = reverse```

6. co b2c a2b a = b2c $ a2b a

7. a _ x = x

8. a' a2b a = a2b a


## Fix it

1. see Sing.hs

2. idem

3. see Arith3Broken.hs


## Type-Kwon-Do

These are eta converted when possible.

1. h = g f

2. e = w q

3. xform (x,y) = (xz x, yz y)

4. munge x2y y2wz = fst y2wz x2y 

