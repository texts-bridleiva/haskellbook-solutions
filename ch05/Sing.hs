module Sing where

-- This code may be difficult to fix for non-westerners. I found it difficult to figure out
-- what the code was supposed to do. My interpretation is that there are two popular songs ie:
-- "Singin in the rain" and "Somewhere over the rainbow" and that fstString and sndString are
-- supposed to return them respectively.

fstString :: [Char] -> [Char]
fstString x = x ++ " in the rain"

sndString :: [Char] -> [Char]
sndString x = x ++ " over the rainbow"

-- There are several ways of changing this function so it sings the other song, the simplest I guess is
-- changing the comparator from ">" to "<"
sing :: String
sing = if (x > y) then fstString x else sndString y
    where 
        x = "Singin"
        y = "Somewhere"
