module WriteCode where


-- #1
tensDigit :: Integral a => a -> a
tensDigit x = d
    where 
        xLast = fst $ divByTen x
        d     = snd $ divByTen xLast
        divByTen = flip divMod 10
    

hunsD :: Integral a => a -> a
hunsD x = d
    where 
        xLast = fst $ flip divMod 100 x
        d     = snd $ flip divMod 10 xLast

-- #2
foldBool :: a -> a -> Bool -> a
foldBool x y b = case b of
    True    -> y
    _       -> x

foldBool2 :: a -> a -> Bool -> a
foldBool2 x y b 
    | b     = y
    | not b = x

foldBool3 :: a -> a -> Bool -> a
foldBool3 x _ False = x
foldBool3 _ y True  = y

-- #3
g :: (a -> b) -> (a, c) -> (b, c)
g a2b (a, c) = (a2b a, c)
