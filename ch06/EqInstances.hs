module EqInstances where

data TisAnInteger = TisAn Integer
data TwoIntegers = Two Integer Integer
data StringOrInt = TisAnInt Int | TisAString String
data Pair a = Pair a a
data Tuple a b = Tuple a b
data Which a = ThisOne a | ThatOne a
data EitherOr a b = Hello a | Goodbye b


instance Eq TisAnInteger where
    (==) (TisAn i1) (TisAn i2) = i1 == i2

instance Eq TwoIntegers where
    (==) (Two a1 b1) (Two a2 b2) = (a1 == a2) && (b1 == b2)

instance Eq StringOrInt where
    (==) (TisAnInt i1) (TisAnInt i2) = i1 == i2
    (==) (TisAString s1) (TisAString s2) = s1 == s2
    (==) _ _ = False    -- This is a matter of opinion, does 10 == "10"?

instance Eq a => Eq (Pair a) where
    (==) (Pair a11 a12) (Pair a21 a22) = a11 == a21 && a12 == a22

instance (Eq a, Eq b) => Eq (Tuple a b) where
    (==) (Tuple a1 b1) (Tuple a2 b2) = a1 == a2 && b1 == b2 

instance Eq a => Eq (Which a) where
    (==) (ThisOne a1) (ThisOne a2) = a1 == a2
    (==) (ThatOne a1) (ThatOne a2) = a1 == a2
    (==) _ _ = False

instance (Eq a, Eq b) => Eq (EitherOr a b) where
    (==) (Hello a1) (Hello a2) = a1 == a2 
    (==) (Goodbye a1) (Goodbye a2) = a1 == a2 
    (==) _ _ = False
