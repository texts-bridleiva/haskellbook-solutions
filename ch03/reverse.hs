module Reverse where

rvrs :: String -> String
rvrs s = awesome ++ is ++ curry
    where
        curry   = take 5 s
        is      = take 4 $ drop 5 s -- includes spaces: " is "
        awesome = drop 9 s


main :: IO ()
main = print $ rvrs "Curry is awesome"
